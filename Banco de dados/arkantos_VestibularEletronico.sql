-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 19-Jul-2020 às 12:37
-- Versão do servidor: 5.7.30
-- versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `arkantos_VestibularEletronico`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `Alternativas`
--

CREATE TABLE `Alternativas` (
  `ID_Alternativas` int(11) NOT NULL,
  `Alternativas` varchar(300) NOT NULL,
  `R_correta` int(11) DEFAULT NULL,
  `ID_Enunciado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Credencial`
--

CREATE TABLE `Credencial` (
  `ID_Credencial` int(11) NOT NULL,
  `Credencial` varchar(255) NOT NULL,
  `ID_Usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Credencial`
--

INSERT INTO `Credencial` (`ID_Credencial`, `Credencial`, `ID_Usuario`) VALUES
(1, 'P4$$word22', 1),
(2, '4567', 2),
(3, '1234', 3),
(4, '7890', 4),
(5, '123', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Cursos`
--

CREATE TABLE `Cursos` (
  `ID_Cursos` int(11) NOT NULL,
  `Nome_Curso` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Cursos`
--

INSERT INTO `Cursos` (`ID_Cursos`, `Nome_Curso`) VALUES
(1, 'Gestão de Recursos Humanos'),
(2, 'Gestão Comercial'),
(3, 'Gestão da Tecnologia da Informação'),
(4, 'Analise e Desenvolvimento de Sistemas'),
(5, 'Administração'),
(6, 'Pedagogia');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Enunciados`
--

CREATE TABLE `Enunciados` (
  `ID_Enunciado` int(11) NOT NULL,
  `Enunciado` varchar(1000) NOT NULL,
  `Tipo` varchar(45) NOT NULL,
  `Data_de_inclusao` date NOT NULL,
  `ID_Alternativas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Motivacional`
--

CREATE TABLE `Motivacional` (
  `ID_Motivacional` int(11) NOT NULL,
  `Texto_Motivacional` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Perfil`
--

CREATE TABLE `Perfil` (
  `ID_Perfil` int(11) NOT NULL,
  `Nome_Perfil` varchar(100) NOT NULL,
  `LV_Perfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Perfil`
--

INSERT INTO `Perfil` (`ID_Perfil`, `Nome_Perfil`, `LV_Perfil`) VALUES
(1, 'Candidato', 1),
(2, 'Secretaria', 2),
(3, 'Diretor', 3),
(4, 'Suporte', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Provas`
--

CREATE TABLE `Provas` (
  `ID_Prova` int(11) NOT NULL,
  `ID_Alternativas` int(11) NOT NULL,
  `ID_Motivacional` int(11) NOT NULL,
  `ID_Tema` int(11) NOT NULL,
  `ID_Usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Telefone`
--

CREATE TABLE `Telefone` (
  `ID_telefone` int(11) NOT NULL,
  `DDD_telefone` int(11) NOT NULL,
  `Telefone` int(11) NOT NULL,
  `ID_Usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Telefone`
--

INSERT INTO `Telefone` (`ID_telefone`, `DDD_telefone`, `Telefone`, `ID_Usuario`) VALUES
(1, 61, 33333333, 1),
(2, 61, 985074333, 2),
(3, 61, 995578888, 3),
(4, 62, 993578888, 4),
(7, 61, 33333333, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Tema_Redacao`
--

CREATE TABLE `Tema_Redacao` (
  `ID_Tema` int(11) NOT NULL,
  `Tema` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Tipo`
--

CREATE TABLE `Tipo` (
  `ID_Tipo` int(11) NOT NULL,
  `Tipo` set('Português','Raciocinio logico','Atualidades') DEFAULT NULL,
  `ID_Enunciado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Usuario`
--

CREATE TABLE `Usuario` (
  `ID_Usuario` int(11) NOT NULL,
  `Nome` varchar(255) NOT NULL,
  `CPF` varchar(14) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `ID_Perfil` int(11) NOT NULL,
  `ID_Cursos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Usuario`
--

INSERT INTO `Usuario` (`ID_Usuario`, `Nome`, `CPF`, `Email`, `ID_Perfil`, `ID_Cursos`) VALUES
(1, 'Paulo Matheus', '000.000.000-51', 'Arkantos2564@gmail.com', 4, NULL),
(2, 'Matheus', '000.000.000-52', '2564@gmail.com', 3, NULL),
(3, 'Paulo', '000.000.000-53', 'Arkantos@gmail.com', 2, NULL),
(4, 'Fraçualdo', '000.000.000-54', 'Françualdo@gmail.com', 1, NULL),
(6, 'Geral', '000.000.000-55', 'a@a', 4, NULL);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `Alternativas`
--
ALTER TABLE `Alternativas`
  ADD PRIMARY KEY (`ID_Alternativas`),
  ADD KEY `R_correta` (`R_correta`),
  ADD KEY `ID_Enunciado` (`ID_Enunciado`);

--
-- Índices para tabela `Credencial`
--
ALTER TABLE `Credencial`
  ADD PRIMARY KEY (`ID_Credencial`),
  ADD KEY `ID_Usuario` (`ID_Usuario`);

--
-- Índices para tabela `Cursos`
--
ALTER TABLE `Cursos`
  ADD PRIMARY KEY (`ID_Cursos`);

--
-- Índices para tabela `Enunciados`
--
ALTER TABLE `Enunciados`
  ADD PRIMARY KEY (`ID_Enunciado`);

--
-- Índices para tabela `Motivacional`
--
ALTER TABLE `Motivacional`
  ADD PRIMARY KEY (`ID_Motivacional`);

--
-- Índices para tabela `Perfil`
--
ALTER TABLE `Perfil`
  ADD PRIMARY KEY (`ID_Perfil`);

--
-- Índices para tabela `Provas`
--
ALTER TABLE `Provas`
  ADD PRIMARY KEY (`ID_Prova`),
  ADD KEY `ID_Alternativas` (`ID_Alternativas`),
  ADD KEY `ID_Motivacional` (`ID_Motivacional`),
  ADD KEY `ID_Tema` (`ID_Tema`),
  ADD KEY `ID_Usuario` (`ID_Usuario`);

--
-- Índices para tabela `Telefone`
--
ALTER TABLE `Telefone`
  ADD PRIMARY KEY (`ID_telefone`),
  ADD KEY `ID_Usuario` (`ID_Usuario`);

--
-- Índices para tabela `Tema_Redacao`
--
ALTER TABLE `Tema_Redacao`
  ADD PRIMARY KEY (`ID_Tema`);

--
-- Índices para tabela `Tipo`
--
ALTER TABLE `Tipo`
  ADD PRIMARY KEY (`ID_Tipo`),
  ADD KEY `ID_Enunciado` (`ID_Enunciado`);

--
-- Índices para tabela `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`ID_Usuario`),
  ADD UNIQUE KEY `CPF` (`CPF`),
  ADD KEY `ID_Perfil` (`ID_Perfil`),
  ADD KEY `ID_Cursos` (`ID_Cursos`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `Alternativas`
--
ALTER TABLE `Alternativas`
  MODIFY `ID_Alternativas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Credencial`
--
ALTER TABLE `Credencial`
  MODIFY `ID_Credencial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `Cursos`
--
ALTER TABLE `Cursos`
  MODIFY `ID_Cursos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `Enunciados`
--
ALTER TABLE `Enunciados`
  MODIFY `ID_Enunciado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Motivacional`
--
ALTER TABLE `Motivacional`
  MODIFY `ID_Motivacional` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Perfil`
--
ALTER TABLE `Perfil`
  MODIFY `ID_Perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `Provas`
--
ALTER TABLE `Provas`
  MODIFY `ID_Prova` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Telefone`
--
ALTER TABLE `Telefone`
  MODIFY `ID_telefone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `Tema_Redacao`
--
ALTER TABLE `Tema_Redacao`
  MODIFY `ID_Tema` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Tipo`
--
ALTER TABLE `Tipo`
  MODIFY `ID_Tipo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `ID_Usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `Alternativas`
--
ALTER TABLE `Alternativas`
  ADD CONSTRAINT `Alternativas_ibfk_1` FOREIGN KEY (`R_correta`) REFERENCES `Alternativas` (`ID_Alternativas`),
  ADD CONSTRAINT `Alternativas_ibfk_2` FOREIGN KEY (`ID_Enunciado`) REFERENCES `Enunciados` (`ID_Enunciado`);

--
-- Limitadores para a tabela `Credencial`
--
ALTER TABLE `Credencial`
  ADD CONSTRAINT `Credencial_ibfk_1` FOREIGN KEY (`ID_Usuario`) REFERENCES `Usuario` (`ID_Usuario`);

--
-- Limitadores para a tabela `Provas`
--
ALTER TABLE `Provas`
  ADD CONSTRAINT `Provas_ibfk_1` FOREIGN KEY (`ID_Alternativas`) REFERENCES `Alternativas` (`ID_Alternativas`),
  ADD CONSTRAINT `Provas_ibfk_2` FOREIGN KEY (`ID_Motivacional`) REFERENCES `Motivacional` (`ID_Motivacional`),
  ADD CONSTRAINT `Provas_ibfk_3` FOREIGN KEY (`ID_Tema`) REFERENCES `Tema_Redacao` (`ID_Tema`),
  ADD CONSTRAINT `Provas_ibfk_4` FOREIGN KEY (`ID_Usuario`) REFERENCES `Usuario` (`ID_Usuario`);

--
-- Limitadores para a tabela `Telefone`
--
ALTER TABLE `Telefone`
  ADD CONSTRAINT `Telefone_ibfk_1` FOREIGN KEY (`ID_Usuario`) REFERENCES `Usuario` (`ID_Usuario`);

--
-- Limitadores para a tabela `Tipo`
--
ALTER TABLE `Tipo`
  ADD CONSTRAINT `Tipo_ibfk_1` FOREIGN KEY (`ID_Enunciado`) REFERENCES `Enunciados` (`ID_Enunciado`);

--
-- Limitadores para a tabela `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`ID_Perfil`) REFERENCES `Perfil` (`ID_Perfil`),
  ADD CONSTRAINT `Usuario_ibfk_2` FOREIGN KEY (`ID_Cursos`) REFERENCES `Cursos` (`ID_Cursos`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
