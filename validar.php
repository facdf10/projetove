<?php
    session_start(); 
        //Incluindo a conexão com banco de dados   
    include_once("classe/conexao.php");    

	if(empty($_POST['email']) || empty($_POST['senha'])) {
		header('location: login.php');
		exit();
	}
$email = mysqli_real_escape_string($mysqli, $_POST['email']);
$senha = mysqli_real_escape_string($mysqli, $_POST['senha']); 

$query = "select u.ID_Usuario as id, u.Email as email, c.Credencial as senha, u.ID_Perfil as id_perfil
               from Usuario as u, Credencial as c 
               where u.ID_Usuario = c.ID_Usuario and 
                     u.email = '{$email}' and c.Credencial = '{$senha}'";

$result = mysqli_query($mysqli, $query);
$row = mysqli_fetch_array($result);

if($row != null) {
    $_SESSION['id_usuario'] = $row['id'];
    $_SESSION['email'] = $row['email'];
    $_SESSION['id_perfil'] = $row['id_perfil'];
	header('location: menu.php');
} else {
    header('location: login.php');
}
?>